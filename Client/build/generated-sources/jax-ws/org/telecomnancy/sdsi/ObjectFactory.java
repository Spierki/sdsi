
package org.telecomnancy.sdsi;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.telecomnancy.sdsi package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PublishResponse_QNAME = new QName("http://sdsi.telecomnancy.org/", "publishResponse");
    private final static QName _Publish_QNAME = new QName("http://sdsi.telecomnancy.org/", "publish");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.telecomnancy.sdsi
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Publish }
     * 
     */
    public Publish createPublish() {
        return new Publish();
    }

    /**
     * Create an instance of {@link PublishResponse }
     * 
     */
    public PublishResponse createPublishResponse() {
        return new PublishResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublishResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sdsi.telecomnancy.org/", name = "publishResponse")
    public JAXBElement<PublishResponse> createPublishResponse(PublishResponse value) {
        return new JAXBElement<PublishResponse>(_PublishResponse_QNAME, PublishResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Publish }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sdsi.telecomnancy.org/", name = "publish")
    public JAXBElement<Publish> createPublish(Publish value) {
        return new JAXBElement<Publish>(_Publish_QNAME, Publish.class, null, value);
    }

}
