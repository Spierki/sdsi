1) Lors du chargement de la page client, un objet Order est crée par le webservice et envoyé dans la queue de message "orderQueue"
2) La classe OrderListenerBean écoute sur cette queue et l'ordre est transmit au Worker
3) Le worker effectue l'action
4) Lorsque le worker a terminé son action il génère le temps de fin de la tâche et replace cette tâche dans une nouvelle queue de message "resultQueue"