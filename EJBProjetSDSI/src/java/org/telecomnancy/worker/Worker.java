/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.telecomnancy.worker;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;

/**
 *
 * @author Céline & Florian
 */

@Stateful
public class Worker {
    @Resource(mappedName = "jms/resultQueue")
    private Queue resultQueue;
    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    private JMSContext context;
 
    public void execute(Order order) {
        try {
            System.out.println("Thread will sleep for :" + String.valueOf(order.getTimeToSleep()) + 
                    "ms, zzzzz......");
            Thread.sleep(order.getTimeToSleep());
            order.setTimeEnd(System.currentTimeMillis());
            System.out.println("Order done !");
            
            ObjectMessage objMsg = context.createObjectMessage();
            objMsg.setObject(order);
            sendJMSMessageToResultQueue(objMsg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }catch (JMSException e) {
            e.printStackTrace();
        }
        
    }

    private void sendJMSMessageToResultQueue(ObjectMessage order) {
        context.createProducer().send(resultQueue, order);
    }
    
    
}
