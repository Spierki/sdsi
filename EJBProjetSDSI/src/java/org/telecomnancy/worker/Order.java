/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.telecomnancy.worker;

import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Céline & Florian
 */
public class Order implements Serializable {
    private int clientId;
    private int timeToSleep;
    private long timeStart;
    private long timeEnd;
    
    public Order(int clientId, int timeToSleep, long timeStart, long timeEnd) {
        this.clientId = clientId;
        this.timeToSleep = timeToSleep;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(long timeEnd) {
        this.timeEnd = timeEnd;
    }

    public int getTimeToSleep() {
        return timeToSleep;
    }

    public void setTimeToSleep(int timeToSleep) {
        this.timeToSleep = timeToSleep;
    }  

    /*
    @Override
    public String toString() {
        return "Order [clientId=" + clientId + ", timeToSleep=" + timeToSleep + ", timeStart="
                + timeStart + ", timeEnd=" + timeEnd + ", request=" + request + "]";
    }*/
}
