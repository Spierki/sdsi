/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.telecomnancy.sdsi;

import org.telecomnancy.worker.Order;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;

/**
 *
 * @author Céline & Florian
 */
@WebService(serviceName = "LoadWebService")
@Stateless()
public class LoadWebService {
    @Resource(mappedName = "jms/orderQueue")
    private Queue orderQueue;
    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    private JMSContext context;
   
    @EJB
    Result result;
    
    private void sendJMSMessageToOrderQueue(ObjectMessage order) {
        context.createProducer().send(orderQueue, order);
    }

    /**
     * Web service operation
     */
    public long publish() {
        System.out.println("Start publishing");
        final int clientId = (int) (Math.random() * 500000);
        int timeToSleep = (int) (Math.random() * 15000);
        System.out.println("Client Id " + clientId); 
        Order order = new Order(clientId, timeToSleep, System.currentTimeMillis(), -1);  
        
        ObjectMessage objMsg = context.createObjectMessage();
        try {     
            objMsg.setObject(order);
            sendJMSMessageToOrderQueue(objMsg);
        } catch (JMSException ex) {
            Logger.getLogger(LoadWebService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 0;
    }
}
