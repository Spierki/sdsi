/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.telecomnancy.sdsi;

import org.telecomnancy.worker.Order;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.Singleton;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 *
 * @author Céline & Florian
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/resultQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class ResultListenerBean implements MessageListener {
    
    @EJB
    Result result;
    
    public ResultListenerBean() {}
    
    @Override
    public void onMessage(Message message) {
        if(message instanceof ObjectMessage){
            try {
                ObjectMessage msg = (ObjectMessage) message;
                Order order = (Order) msg.getObject(); 
                result.setClientId(order.getClientId());
                result.setTime(order.getTimeEnd() - order.getTimeStart());
                System.out.println("resultQueue " + order.getClientId());
                System.out.println(result.getTime());
            } catch (JMSException e) {
                e.printStackTrace();
            } 
        }
    }
}
